QT += core
QT -= gui

TARGET = deetectface2
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

TEMPLATE = app

SOURCES += main.cpp
LIBS += `pkg-config opencv --libs`

