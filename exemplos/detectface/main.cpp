#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <thread>
#define CAMERA_IP "http://192.168.0.101/mjpg/video.mjpg"
#define FACE_CASCADE_NAME "/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_alt.xml"
#define WINDOW_NAME "Capture - Face detection"

using namespace std;
using namespace cv;

// Function Headers
void detectAndDisplay(Mat frame);

CascadeClassifier face_cascade;

int filenumber; // Number of file to be saved
string filename;


typedef struct{
    Mat image;
    vector<Rect> faces;
}DetectFaces;

DetectFaces dfaces;
VideoCapture capture;   //VideoCapture capture(0); para webcam
Mat frame;  // Read the video stream

// Function to count the detected faces in your image
void detectFacesInImage(){
    while (true){
        //dfaces.image = frame;

    //    Mat frame_gray;
    //    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    //    equalizeHist(frame_gray, frame_gray);

        // Detect faces
        face_cascade.detectMultiScale(frame, dfaces.faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
        //face_cascade.detectMultiScale(frame, dfaces.faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
        //sleep(1);
    }


}

void display(){
    if (!dfaces.image.empty()){
        imshow("original", dfaces.image);
        //usleep(100);
    }

}

void drawFaces(){
    //Mat crop;
    //Mat res;
    //Mat gray;
//    string text;
//    stringstream sstm;

    cv::Rect roi_b;
    cv::Rect roi_c;

    size_t ic = 0; // ic is index of current element
    int ac = 0; // ac is area of current element

    size_t ib = 0; // ib is index of biggest element
    int ab = 0; // ab is area of biggest element

    // Iterate through all current elements (detected faces)
    for (ic = 0; ic <  dfaces.faces.size(); ic++){
        roi_c.x = dfaces.faces[ic].x;
        roi_c.y = dfaces.faces[ic].y;
        roi_c.width = (dfaces.faces[ic].width);
        roi_c.height = (dfaces.faces[ic].height);

        ac = roi_c.width * roi_c.height; // Get the area of current element (detected face)

        roi_b.x = dfaces.faces[ib].x;
        roi_b.y = dfaces.faces[ib].y;
        roi_b.width = (dfaces.faces[ib].width);
        roi_b.height = (dfaces.faces[ib].height);

        ab = roi_b.width * roi_b.height; // Get the area of biggest element, at beginning it is same as "current" element

        if (ac > ab){
            ib = ic;
            roi_b.x = dfaces.faces[ib].x;
            roi_b.y = dfaces.faces[ib].y;
            roi_b.width = (dfaces.faces[ib].width);
            roi_b.height = (dfaces.faces[ib].height);
        }

        //crop = dfaces.image(roi_b);
        //resize(crop, res, Size(128, 128), 0, 0, INTER_LINEAR); // This will be needed later while saving images
        //cvtColor(crop, gray, CV_BGR2GRAY); // Convert cropped image to Grayscale

        // Form a filename
//        filename = "";
//        stringstream ssfn;
//        ssfn << filenumber << ".png";
//        filename = ssfn.str();
//        filenumber++;

//        imwrite(filename, gray);

        Point pt1(dfaces.faces[ic].x, dfaces.faces[ic].y); // Display detected faces on main window - live stream from camera
        Point pt2((dfaces.faces[ic].x + dfaces.faces[ic].height), (dfaces.faces[ic].y + dfaces.faces[ic].width));
        rectangle(dfaces.image, pt1, pt2, Scalar(0, 255, 0), 2, 8, 0);
    }

// Show image
    //sstm << "Crop area size: " << roi_b.width << "x" << roi_b.height << " Filename: " << filename;
    //text = sstm.str();

    //putText(frame, text, cvPoint(30, 30), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0, 0, 255), 1, CV_AA);
    display();

//    if (!crop.empty())
//    {
//        imshow("detected", crop);
//    }
//    else
//        destroyWindow("detected");
}


void saveImage(Mat frame, string imagePath){
    if (!frame.empty()){
        imwrite(imagePath, frame);
//        Mat image;
//        image = imread( "teste.jpeg", 1 );
//        //cout << countFacesInImage(frame) << endl;
    }
}

Mat readImage(string imagePath){
    Mat image;
    image = imread(imagePath, 1 );
    return image;
}



// Function main
int main(void){
//    auto t1 = std::chrono::high_resolution_clock::now();
//    auto t2 =  std::chrono::high_resolution_clock::now();
//    std::chrono::duration<double> tempo = t2 - t1;

//    t1 = std::chrono::high_resolution_clock::now();





    try{
        capture.open(CAMERA_IP);
        if (!capture.isOpened()){
            throw(1);
        }

        if (!face_cascade.load(FACE_CASCADE_NAME)){
            printf("--(!)Error loading\n");
            throw(2);
        };

    }catch (int e){
        if(e==1){
            cout << "Ocorreu um erro com o IP da câmera: " << CAMERA_IP << endl;
        }else if(e == 2){
            cout << "Ocorreu um erro com a localização do arquivo de padrão de detecção facial: " << FACE_CASCADE_NAME << endl;
        }

        return -1;
    }
//    t2 =  std::chrono::high_resolution_clock::now();
//    tempo = t2 - t1;
//    cout << "Tempo de criar capture : " << tempo.count() << endl;



//    t1 = std::chrono::high_resolution_clock::now();

    //Salva a imagem
    capture >> frame;

//    t2 =  std::chrono::high_resolution_clock::now();
//    tempo = t2 - t1;
//    cout << "Tempo para salvar a imagem em Mat : " << tempo.count() << endl;

    thread t1(detectFacesInImage);

    thread t0(display);



    while(true){
        capture >> frame;
        capture >> dfaces.image;
        if (!frame.empty()){


            //t1 = std::chrono::high_resolution_clock::now();

            //detectFacesInImage(frame);

            //t2 =  std::chrono::high_resolution_clock::now();
            //tempo = t2 - t1;
            //cout << "Tempo para detectar faces : " << tempo.count() << endl;

            //t1 = std::chrono::high_resolution_clock::now();

            drawFaces();

            //t2 =  std::chrono::high_resolution_clock::now();
            //tempo = t2 - t1;
            //cout << "Tempo para Desenhar na tela : " << tempo.count() << endl;

            //usleep(10);
        }
         //usleep(1000);
//        else{
//            printf(" --(!) No captured frame -- Break!");
//            break;
//        }

        int c = waitKey(10);

//        if (27 == char(c)){
//            break;
//        }
    }
    t0.join();
    t1.join();
    //t2.join();
    return 0;
}
