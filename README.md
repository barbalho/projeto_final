# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Instalação do OpenCV na BeagleBone Black ###


Configurações de Host
sudo su
iptables --table nat --append POSTROUTING --out-interface wlan0 -j MASQUERADE
iptables --append FORWARD --in-interface eth1 -j ACCEPT
echo 1 > /proc/sys/net/ipv4/ip_forward


Configurações na Beagle
sudo su
route add default gw 192.168.7.1
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
/etc/init.d/networking restart

Fonte do processo de instalação:
http://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html?highlight=installation

Instalação de Dependências 
[compiler] sudo apt-get install build-essential
[required] sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
[optional] sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev



Download OpenCV: https://sourceforge.net/projects/opencvlibrary/
opencv-3.1.0.zip


unzip opencv-3.1.0.zip
cd opencv-3.1.0/
mkdir release
cd release
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..

make
sudo make install

O processo durou ao todo 7 horas