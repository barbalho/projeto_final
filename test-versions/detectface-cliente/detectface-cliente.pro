QT += core
QT -= gui

TARGET = detectface-cliente
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp
CONFIG += c++11
LIBS += `pkg-config opencv --libs`

