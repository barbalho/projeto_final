#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <thread>
#include <csignal>
#include <arpa/inet.h>  //inet_addr
#define CAMERA_IP "http://192.168.0.101/mjpg/video.mjpg"
#define FACE_CASCADE_NAME "/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_alt.xml"
#define WINDOW_NAME "Capture - Face detection"

#define PORTNUM 4325
#define IP_SERV "127.0.0.1"
//#define IP_SERV "192.168.7.1"

using namespace std;
using namespace cv;

// Function Headers
void detectAndDisplay(Mat frame);

CascadeClassifier face_cascade;

int filenumber; // Number of file to be saved
string filename;

typedef struct{
    Mat image; //face para registrar
    int id; // Idetinficaçaõ da camera
    cv::Rect roi_b; //proporção da tela
}Faces;

typedef struct{
    Mat image;
    vector<Rect> faces;
}DetectFaces;

DetectFaces dfaces;
VideoCapture capture;   //VideoCapture capture(0); para webcam
Mat frame;  // Read the video stream

//void sendMensage(Faces mensagem){
//    struct sockaddr_in endereco;
//    int socketId;
//    int bytesenviados;
//    memset(&endereco, 0, sizeof(endereco));
//    endereco.sin_family = AF_INET;
//    endereco.sin_port = htons(PORTNUM);
//    endereco.sin_addr.s_addr = inet_addr(IP_SERV);
//    socketId = socket(AF_INET, SOCK_STREAM, 0);

//    if (socketId == -1){
//        printf("Falha ao executar socket()\n");
//    }

//    if ( connect (socketId, (struct sockaddr *)&endereco, sizeof(struct sockaddr)) == -1 ){
//        printf("Falha ao executar connect()\n");
//    }

//    bytesenviados = send(socketId,&mensagem,sizeof(mensagem),0);

//    if (bytesenviados == -1){
//        printf("Falha ao executar send()");
//    }
//    close(socketId);
//}
bool connectSocket(int *socketId){
    struct sockaddr_in endereco;

    memset(&endereco, 0, sizeof(endereco));
    endereco.sin_family = AF_INET;
    endereco.sin_port = htons(PORTNUM);
    endereco.sin_addr.s_addr = inet_addr(IP_SERV);
    *socketId = socket(AF_INET, SOCK_STREAM, 0);

    if (*socketId == -1){
        printf("Falha ao executar socket()\n");
        close(*socketId);
        return false;
    }

    if ( connect (*socketId, (struct sockaddr *)&endereco, sizeof(struct sockaddr)) == -1 ){
        printf("Falha ao executar connect()\n");
        close(*socketId);
        return false;
    }
    return true;
}

void sendMensage(Faces mensagem){
    int socketId;
    int bytesenviados;
    if (connectSocket(&socketId)){
        bytesenviados = send(socketId,&mensagem,sizeof(mensagem),0);
        cout << sizeof(mensagem) << endl;
        if (bytesenviados == -1){
            cout << "Falha ao executar send()" << endl;
            return;
        }
        cout << "Mensagem Enviada" << endl;
    }else{
        cout << "Connect to the server before sending data" << endl;
    }
}

void saveImage(Mat frame, string imagePath){
    if (!frame.empty()){
        imwrite(imagePath, frame);
    }
}

Mat readImage(string imagePath){
    Mat image;
    image = imread(imagePath, 1 );
    return image;
}

// Function to count the detected faces in your image
void detectFacesInImage(){
    while (true){
        //dfaces.image = frame;

    //    Mat frame_gray;
    //    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    //    equalizeHist(frame_gray, frame_gray);

        // Detect faces
        face_cascade.detectMultiScale(frame, dfaces.faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
        //face_cascade.detectMultiScale(frame, dfaces.faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
        //sleep(1);
    }


}

void drawFaces(){
    if (dfaces.faces.size() == 1){
        Mat crop;
        Mat res;
        Mat gray;

        cv::Rect roi_b;
        cv::Rect roi_c;

        size_t ic = 0; // ic is index of current element
        int ac = 0; // ac is area of current element

        size_t ib = 0; // ib is index of biggest element
        int ab = 0; // ab is area of biggest element

        // Iterate through all current elements (detected faces)
        for (ic = 0; ic <  dfaces.faces.size(); ic++){
            roi_c.x = dfaces.faces[ic].x;
            roi_c.y = dfaces.faces[ic].y;
            roi_c.width = (dfaces.faces[ic].width);
            roi_c.height = (dfaces.faces[ic].height);

            ac = roi_c.width * roi_c.height; // Get the area of current element (detected face)

            roi_b.x = dfaces.faces[ib].x;
            roi_b.y = dfaces.faces[ib].y;
            roi_b.width = (dfaces.faces[ib].width);
            roi_b.height = (dfaces.faces[ib].height);

            ab = roi_b.width * roi_b.height; // Get the area of biggest element, at beginning it is same as "current" element

            if (ac > ab){
                ib = ic;
                roi_b.x = dfaces.faces[ib].x;
                roi_b.y = dfaces.faces[ib].y;
                roi_b.width = (dfaces.faces[ib].width);
                roi_b.height = (dfaces.faces[ib].height);
            }
            if(roi_b.width < 200){
                return;
            }

            crop = dfaces.image(roi_b);
            resize(crop, res, Size(128, 128), 0, 0, INTER_LINEAR); // This will be needed later while saving images
            cvtColor(crop, gray, CV_BGR2GRAY); // Convert cropped image to Grayscale

            saveImage(gray, "face.png");

            //Inicia procolo de envio
            Faces face;
            face.id = 0;
            face.roi_b = roi_b;
            face.image = gray;
            sendMensage(face);
            sleep(4);
        }
    }else if (dfaces.faces.size() > 1){
        cout << "Duas faces detectadas, apenas uma é permitida" << endl;
    }else{

    }

}







// Function main
int main(void){

    try{
        capture.open(CAMERA_IP);
        if (!capture.isOpened()){
            throw(1);
        }

        if (!face_cascade.load(FACE_CASCADE_NAME)){
            printf("--(!)Error loading\n");
            throw(2);
        };

    }catch (int e){
        if(e==1){
            cout << "Ocorreu um erro com o IP da câmera: " << CAMERA_IP << endl;
        }else if(e == 2){
            cout << "Ocorreu um erro com a localização do arquivo de padrão de detecção facial: " << FACE_CASCADE_NAME << endl;
        }
        return -1;
    }

    //Carrega um frame da câmera
    capture >> frame;

    //Fica verificando por faces no frame
    thread t1(detectFacesInImage);

    while(true){
        capture >> frame;
        capture >> dfaces.image;
        if (!frame.empty()){

            drawFaces();
        }

        int c = waitKey(10);

        if (27 == char(c)){
            break;
        }
    }

    t1.join();
    return 0;
}
