#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::saveImage(Mat frame, string imagePath){
    if (!frame.empty()){
        imwrite(imagePath, frame);
        cout << "Salvou" << endl;
    }else{
        cout << "frame vazio" << endl;
    }
}

void MainWindow::socketHandler(int socketDescriptor, Faces mensagem) {
    int byteslidos;

    if (socketDescriptor == -1) {
        printf("Falha ao executar accept().");
        exit(EXIT_FAILURE);
    }

    byteslidos = recv(socketDescriptor,&mensagem,sizeof(mensagem),0);
    cout << byteslidos << endl;

    this->mensagem = mensagem;

    saveImage(this->mensagem.image, "imagem.png");

    if (byteslidos == -1) {
        printf("Falha ao executar recv()");
        exit(EXIT_FAILURE);
    }
    else if (byteslidos == 0) {
        printf("Cliente finalizou a conexão\n");
    }

    ::close(socketDescriptor);
}

void MainWindow::server() {
    struct sockaddr_in endereco;

    /*Variáveis relacionadas com as conexões clientes*/
    struct sockaddr_in enderecoCliente;
    socklen_t tamanhoEnderecoCliente = sizeof(struct sockaddr);
    int conexaoClienteId;

    Faces mensagem;

    memset(&endereco, 0, sizeof(endereco));
    endereco.sin_family = AF_INET;
    endereco.sin_port = htons(PORTNUM);
    endereco.sin_addr.s_addr = inet_addr(IP_SERV);

    socketId = socket(AF_INET, SOCK_STREAM, 0);

    if (socketId == -1) {
        printf("Falha ao executar socket().\n");
        exit(EXIT_FAILURE);
    }

    if (bind(socketId, (struct sockaddr *)&endereco, sizeof(struct sockaddr)) == -1 ) {
        printf("Falha ao executar bind().\n");
        exit(EXIT_FAILURE);
    }

    if (listen( socketId, 10 ) == -1) {
        printf("Falha ao executar listen().\n");
        exit(EXIT_FAILURE);
    }

    /*Servidor ficar em um loop infinito*/
    while(true) {
        cout << "1" << endl;

        printf("Servidor: esperando conexões clientes\n");
        /*Servidor fica bloqueado esperando uma conexão do cliente*/
        conexaoClienteId = accept(socketId,(struct sockaddr *) &enderecoCliente, &tamanhoEnderecoCliente);
        printf("Servidor: recebeu conexão de %s\n", inet_ntoa(enderecoCliente.sin_addr));
        threadServer = std::thread(&MainWindow::socketHandler,this, conexaoClienteId, mensagem);
        threadServer.detach();
        sleep(1);
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    /*Inicializaçã do servidor para escutar o cliente*/
    threadStartServer = std::thread(&MainWindow::server,this);
}

MainWindow::~MainWindow(){
    delete ui;
    ::close(socketId);
    threadServer.join();
    threadStartServer.join();
}
