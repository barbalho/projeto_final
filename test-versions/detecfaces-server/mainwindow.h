#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <QMainWindow>
#include <thread>
#include <chrono>
//#include <cstdio>
//#include <cstring>
//#include <cstdlib>
//#include <netinet/in.h>
//#include <thread>
//#include <sys/socket.h>
#include <QMainWindow>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <vector>


#define PORTNUM 4325
//#define IP_SERV "192.168.7.1"
#define IP_SERV "127.0.0.1"
#define MAXMSG 1024
#define MAXNAME 100

using namespace std;
using namespace cv;

typedef struct{
    Mat image; //face para registrar
    int id; // Idetinficaçaõ da camera
    cv::Rect roi_b; //proporção da tela
}Faces;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void socketHandler(int socketDescriptor, Faces mensagem);
    void server();
    void saveImage(Mat frame, string imagePath);

private:
    std::thread threadServer;
    std::thread threadStartServer;
    Ui::MainWindow *ui;
    Faces mensagem;
    int socketId;
};

#endif // MAINWINDOW_H
