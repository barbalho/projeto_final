#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>

using namespace std;

#define PORTNUM 4325
#define IP_SERV "192.168.7.1"
#define MAXMSG 1024
#define MAXNAME 100

namespace Ui {
    class MainWindow;
}

struct Message {
    public:
        int   trainID;
        float speed;
        bool  travado;
};

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();
    void socketHandler(int socketDescriptor, Message message);
    void server();

private:
    Ui::MainWindow *ui;
    bool           connected;
    time_t         time_conection;
    Message        message;
    int            socketId;
    std::thread    threadServer;
    std::thread    threadStartServer;
};

#endif // MAINWINDOW_H
