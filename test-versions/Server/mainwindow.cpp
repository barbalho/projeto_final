#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

using namespace std;

/**
 * Constructor.
 */
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    connected = false;
    // Inicializaçã do servidor para escutar o cliente
    threadStartServer = std::thread(&MainWindow::server, this);
}

/**
 * Destructor.
 */
MainWindow::~MainWindow() {
    delete ui;
    ::close(socketId);
    threadServer.join();
    threadStartServer.join();
}

/**
 * Socket Handler.
 */
void MainWindow::socketHandler(int socketDescriptor, Message message) {
    // Received bytes
    int receivedBytes;

    // Check if the socket descriptor is fail
    if (socketDescriptor == -1) {
        cout << "Falha ao executar accept()." << endl;
        exit(EXIT_FAILURE);
    }

    // Receive bytes
    receivedBytes = recv(socketDescriptor, &message, sizeof(message), 0);
    cout << receivedBytes << endl;

    // Receive the message
    this->message = message;

    // Check if the received bytes is valid
    if (receivedBytes == -1) {
        cout << "Falha ao executar recv()" << endl;
        exit(EXIT_FAILURE);
    } else if (receivedBytes == 0) {
        cout << "Cliente finalizou a conexão\n" << endl;
    }
    // Close connection
    ::close(socketDescriptor);
}

/**
 * Server.
 */
void MainWindow::server() {
    //
    struct sockaddr_in address;

    // Variáveis relacionadas com as conexões clientes
    struct sockaddr_in clientAddress;
    socklen_t clientAddressSize = sizeof(struct sockaddr);
    int clinetConnectionID;

    Message message;

    memset(&address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_port = htons(PORTNUM);
    address.sin_addr.s_addr = inet_addr(IP_SERV);

    socketId = socket(AF_INET, SOCK_STREAM, 0);

    if (socketId == -1) {
        cout << "Falha ao executar socket()." << endl;
        exit(EXIT_FAILURE);
    }

    if (bind(socketId, (struct sockaddr *) &address, sizeof(struct sockaddr)) == -1 ) {
        cout << "Falha ao executar bind()." << endl;
        exit(EXIT_FAILURE);
    }

    if (listen( socketId, 10 ) == -1) {
        cout << "Falha ao executar listen()." << endl;
        exit(EXIT_FAILURE);
    }

    // Servidor ficar em um loop infinito
    while(true) {
        cout << "Servidor: esperando conexões clientes." << endl;

        // Servidor fica bloqueado esperando uma conexão do cliente
        clinetConnectionID = accept(socketId, (struct sockaddr *) &clientAddress, &clientAddressSize);
        cout << "Servidor: recebeu conexão de " << inet_ntoa(clientAddress.sin_addr) << endl;
        threadServer = std::thread(&MainWindow::socketHandler, this, clinetConnectionID, message);
        threadServer.detach();
        sleep(1);
    }
}
